// Model Options
enum Option {
  data15 = 1,
  sgn_VBF = 2,
  bkg_QCD = 3,
  bkg_top = 4,
  bkg_Wstrong = 5,
  bkg_Zstrong = 6,
  bkg_WEWK = 7,
  bkg_ZEWK = 8,
//  bkg_VV = 9
};
