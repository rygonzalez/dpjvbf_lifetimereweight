// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "AnalysisInput.h"

AnalysisInput::AnalysisInput(Option option)
{
  
  //std::cout << std::endl;
  std::cout << "--- AnalysisInput :: Analysis on " << option;

  option_ = option;

  if ( option_ == data15  )  std::cout << ": Data (data15) ---" <<std::endl;
  if ( option_ == sgn_VBF )  std::cout << ": VBF signal ---" <<std::endl;
  if ( option_ == bkg_QCD )  std::cout << ": QCD background ---" <<std::endl;
  //if ( option_ == bkg_VV   )   std::cout << ": Diboson background" <<std::endl;
  if ( option_ == bkg_top   )  std::cout << ": Top background ---" << std::endl;
  if ( option_ == bkg_Wstrong )  std::cout << ": W strong background ---" << std::endl;
  if ( option_ == bkg_Zstrong )  std::cout << ": Z strong background ---" << std::endl;
  if ( option_ == bkg_WEWK   )  std::cout << ": W+jj EWK background ---" << std::endl;
  if ( option_ == bkg_ZEWK   )  std::cout << ": Z+jj EWK background ---" << std::endl;

}


AnalysisInput::~AnalysisInput()
{
  
  //delete input;
  delete chain_in;
  delete output;
  //delete tree_out;

}

void AnalysisInput::initialize()
{

  // Tree integer variables (no vectors) 
  n_events = 0;
  //accepted_events = 0;

  // Lifetime reweighting
  ct1 = 0;
  ct2 = 0;
  w1 = 0;
  w2 = 0;

  // csv Files
  //if ( option_ == sgn_VBF ) {
    //MCEv_VBF.open("../data/MCEv_VBF.csv");
    //MCEv_VBF << "Selection" << "," << "Events" << std::endl;

    //Ev_Run2.open("../data/Ev_Run2.csv");
    //Ev_Run2 << "Selection" << "," << "Events" << std::endl;
  //}

}

float AnalysisInput::notZero(float numerator, float denominator) {

  if ( denominator == 0) n_zero++;//std::cout << "Divided by zero. -checked div";
  else return numerator / denominator;
  
}

float AnalysisInput::ctau(float partX, float partY, float partZ, float partE, float partM) {
  
  float decayLength = TMath::Sqrt( pow(partX,2) + pow(partY,2) + pow(partZ,2) );
  float gamma = notZero(partE/1000.0,partM);
  float beta = TMath::Sqrt( 1 - 1/pow(gamma, 2) );
  float beta_gamma = beta*gamma;
  float ctau = notZero(decayLength, beta_gamma);
  return ctau;
      
}

float AnalysisInput::newWeight(float t_gen, float t_new, float t_i) {

  return (t_gen/t_new)*exp(-((1/t_new)-(1/t_gen))*t_i);
  
}

void AnalysisInput::addBranches()
{

  if ( option_ == data15 )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/data15_ntuple.root";
  if ( option_ == sgn_VBF )  outname = "/home/richards/WorkArea/DPJanalysis/repositories/DPJVBF_LifetimeReweight/samples/VBF_sgn_ntuple.root";
  if ( option_ == bkg_QCD )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/QCD_bkg_ntuple.root";
  //if ( option_ == bkg_VV )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DarkPhotonJetVBF/samples/samples/VV_bkg_ntuple.root";
  if ( option_ == bkg_top )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/Top_bkg_ntuple.root";
  if ( option_ == bkg_Wstrong )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/Wstrong_bkg_ntuple.root";
  if ( option_ == bkg_Zstrong )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/Zstrong_bkg_ntuple.root";
  if ( option_ == bkg_WEWK )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/WEWK_bkg_ntuple.root";
  if ( option_ == bkg_ZEWK )  outname = "/home/richards/Documents/HEP/DarkPhoton_Analysis/Repositories/DPJVBF_LifetimeReweight/samples/ZEWK_bkg_ntuple.root";

  output = new TFile( outname , "RECREATE");
  tree_out = new TTree("Selection", "Selection");
  tree_out->Branch("weight", &m_weight);
  tree_out->Branch("dsid", &dsid);
  tree_out->Branch("metTrig", &metTrig);
  tree_out->Branch("jet1_pt", &jet1_pt);
  tree_out->Branch("jet1_eta", &jet1_eta);
  tree_out->Branch("jet1_phi", &jet1_phi);
  tree_out->Branch("jet1_e", &jet1_e);
  tree_out->Branch("jet2_pt", &jet2_pt);
  tree_out->Branch("jet2_eta", &jet2_eta);
  tree_out->Branch("jet2_phi", &jet2_phi);
  tree_out->Branch("jet2_e", &jet2_e);
  tree_out->Branch("njet30", &njet30);
  tree_out->Branch("mjj", &mjj);
  tree_out->Branch("detajj", &detajj);
  tree_out->Branch("signetajj", &signetajj);
  tree_out->Branch("absdphijj", &absdphijj);
  tree_out->Branch("hasBjet", &hasBjet);
  tree_out->Branch("dphi_j1met", &dphi_j1met);
  tree_out->Branch("min_dphi_jetmet", &min_dphi_jetmet);
  tree_out->Branch("nLJ20", &nLJ20);
  tree_out->Branch("nLJjets20", &nLJjets20);
  tree_out->Branch("LJjet1_pt", &LJjet1_pt);
  tree_out->Branch("LJjet1_eta", &LJjet1_eta);
  tree_out->Branch("LJjet1_phi", &LJjet1_phi);
  tree_out->Branch("LJjet1_m", &LJjet1_m);
  tree_out->Branch("LJjet1_width", &LJjet1_width);
  tree_out->Branch("LJjet1_EMfrac", &LJjet1_EMfrac);
  tree_out->Branch("LJjet1_timing", &LJjet1_timing);
  tree_out->Branch("LJjet1_jvt", &LJjet1_jvt);
  tree_out->Branch("LJjet1_gapRatio", &LJjet1_gapRatio);
  tree_out->Branch("LJjet1_BIBtagger", &LJjet1_BIBtagger);
  tree_out->Branch("LJjet1_DPJtagger", &LJjet1_DPJtagger);
  tree_out->Branch("LJjet1_isoID", &LJjet1_isoID);
  tree_out->Branch("LJjet1_truthDPidx", &LJjet1_truthDPidx);
  tree_out->Branch("LJ1_type", &LJ1_type);
  tree_out->Branch("MET", &MET);
  tree_out->Branch("METsig", &METsig);
  tree_out->Branch("METOSqrtHT", &METOSqrtHT);
  tree_out->Branch("nLJmus20", &nLJmus20);
  tree_out->Branch("neleSignal", &neleSignal);
  tree_out->Branch("nmuSignal", &nmuSignal);
  tree_out->Branch("neleBaseline", &neleBaseline);
  tree_out->Branch("nmuBaseline", &nmuBaseline);

  if ( option_ == data15 )  std::cout << "--- Select data15 sample ---" << std::endl;
  if ( option_ == sgn_VBF )  std::cout << "--- Select VBF signal sample ---" << std::endl;
  if ( option_ == bkg_QCD )  std::cout << "--- Select QCD background sample ---" << std::endl;
  //if ( option_ == bkg_VV    )  std::cout << "--- Select diboson background sample" << std::endl;
  if ( option_ == bkg_top   )  std::cout << "--- Select Top background sample ---" << std::endl;
  if ( option_ == bkg_Wstrong )  std::cout << "--- Select W strong background sample ---" << std::endl;
  if ( option_ == bkg_Zstrong )  std::cout << "--- Select Z strong background sample ---" << std::endl;
  if ( option_ == bkg_WEWK   )  std::cout << "--- Select W+jj EWK background sample ---" << std::endl;
  if ( option_ == bkg_ZEWK   )  std::cout << "--- Select Z+jj EWK background sample ---" << std::endl;

  // Load input files
  chain_in = new TChain("miniT");

  if ( option_ == data15  ) {
    chain_in->Add(filesFolder + "data15.root");
  }

  if ( option_ == sgn_VBF  ) { // CHANGE FOR LIFETIME REWEIGHTING
    //chain_in->Add(filesFolder + "frvz_vbf_500757.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500758.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500759.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500760.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500761.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500762.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500763.root");
    //chain_in->Add(filesFolder + "frvz_vbf_500764.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521257.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521258.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521259.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521260.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521261.root");
    //chain_in->Add(filesFolder + "frvz_vbf_521262.root");
    chain_in->Add(filesFolder + "frvz_vbf_521263.root");
  }

  if ( option_ == bkg_QCD  ) {
    chain_in->Add(filesFolder + "qcd_main.root");
  }

  //if ( option_ == bkg_VV  ) {
    //chain_in->Add(filesFolder + "diboson_sherpa221.root");
  //}

  if ( option_ == bkg_top  ) {
    chain_in->Add(filesFolder + "top_powheg.root");
  }

  if ( option_ == bkg_Wstrong  ) {
    chain_in->Add(filesFolder + "wjets_strong_sh227.root");
  }

  if ( option_ == bkg_Zstrong  ) {
    chain_in->Add(filesFolder + "zjets_strong_sh227.root");
  }

  if ( option_ == bkg_WEWK  ) {
    chain_in->Add(filesFolder + "wjj_ewk.root");
  }

  if ( option_ == bkg_ZEWK  ) {
    chain_in->Add(filesFolder + "zjj_ewk.root");
  }


  chain_in->SetBranchAddress("scale1fb", &scale1fb);
  chain_in->SetBranchAddress("intLumi", &intLumi);
  chain_in->SetBranchAddress("metTrig_weight", &metTrig_weight);
  chain_in->SetBranchAddress("dsid", &dsid);
  chain_in->SetBranchAddress("metTrig", &metTrig);
  chain_in->SetBranchAddress("jet1_pt", &jet1_pt);
  chain_in->SetBranchAddress("jet1_eta", &jet1_eta);
  chain_in->SetBranchAddress("jet1_phi", &jet1_phi);
  chain_in->SetBranchAddress("jet1_e", &jet1_e);
  chain_in->SetBranchAddress("jet2_pt", &jet2_pt);
  chain_in->SetBranchAddress("jet2_eta", &jet2_eta);
  chain_in->SetBranchAddress("jet2_phi", &jet2_phi);
  chain_in->SetBranchAddress("jet2_e", &jet2_e);
  chain_in->SetBranchAddress("njet30", &njet30);
  chain_in->SetBranchAddress("mjj", &mjj);
  chain_in->SetBranchAddress("detajj", &detajj);
  chain_in->SetBranchAddress("signetajj", &signetajj);
  chain_in->SetBranchAddress("dphijj", &dphijj);
  chain_in->SetBranchAddress("hasBjet", &hasBjet);
  chain_in->SetBranchAddress("dphi_j1met", &dphi_j1met);
  chain_in->SetBranchAddress("min_dphi_jetmet", &min_dphi_jetmet);
  chain_in->SetBranchAddress("nLJ20", &nLJ20);
  chain_in->SetBranchAddress("nLJjets20", &nLJjets20);
  chain_in->SetBranchAddress("LJjet1_pt", &LJjet1_pt);
  chain_in->SetBranchAddress("LJjet1_eta", &LJjet1_eta);
  chain_in->SetBranchAddress("LJjet1_phi", &LJjet1_phi);
  chain_in->SetBranchAddress("LJjet1_m", &LJjet1_m);
  chain_in->SetBranchAddress("LJjet1_width", &LJjet1_width);
  chain_in->SetBranchAddress("LJjet1_EMfrac", &LJjet1_EMfrac);
  chain_in->SetBranchAddress("LJjet1_timing", &LJjet1_timing);
  chain_in->SetBranchAddress("LJjet1_jvt", &LJjet1_jvt);
  chain_in->SetBranchAddress("LJjet1_gapRatio", &LJjet1_gapRatio);
  chain_in->SetBranchAddress("LJjet1_BIBtagger", &LJjet1_BIBtagger);
  chain_in->SetBranchAddress("LJjet1_DPJtagger", &LJjet1_DPJtagger);
  chain_in->SetBranchAddress("LJjet1_isoID", &LJjet1_isoID);
  chain_in->SetBranchAddress("LJjet1_truthDPidx", &LJjet1_truthDPidx);
  chain_in->SetBranchAddress("LJ1_type", &LJ1_type);
  chain_in->SetBranchAddress("MET", &MET);
  chain_in->SetBranchAddress("METsig", &METsig);
  chain_in->SetBranchAddress("METOSqrtHT", &METOSqrtHT);
  chain_in->SetBranchAddress("nLJmus20", &nLJmus20);
  chain_in->SetBranchAddress("neleSignal", &neleSignal);
  chain_in->SetBranchAddress("nmuSignal", &nmuSignal);
  chain_in->SetBranchAddress("neleBaseline", &neleBaseline);
  chain_in->SetBranchAddress("nmuBaseline", &nmuBaseline);
  chain_in->SetBranchAddress("truthPdgId", &truthPdgId); //truth information, vectors
  chain_in->SetBranchAddress("truthE", &truthE); //truth information
  chain_in->SetBranchAddress("truthDecayVtx_x", &truthDecayVtx_x); //truth information
  chain_in->SetBranchAddress("truthDecayVtx_y", &truthDecayVtx_y); //truth information
  chain_in->SetBranchAddress("truthDecayVtx_z", &truthDecayVtx_z); //truth information
  chain_in->SetBranchAddress("truthDecayType", &truthDecayType); //truth information
  chain_in->SetBranchAddress("truthPt", &truthPt); // for LR test

}

void AnalysisInput::execute(float newctau)
{

  // Boost - Progress bar
  n_events = chain_in->GetEntries();
  std::cout << "--- Processing: " << n_events << " events ---" << std::endl;
  //boost::progress_display show_progress( n_events ); // INCLUDE THIS ONE
  //boost::timer::progress_display show_progress( n_events );

  for (Long64_t ievt=0; ievt < n_events ;ievt++) {
    
    // Read branches per event
    chain_in->GetEntry(ievt);
    //++show_progress; // INCLUDE THIS ONE

    //std::cout << truthPdgId->size() << std::endl;
    //std::cout << truthPdgId->at(0) << std::endl;

    std::vector<Float_t> truth_pT; // for LR test

    std::vector<Float_t> truth_x;
    std::vector<Float_t> truth_y;
    std::vector<Float_t> truth_z;
    std::vector<Float_t> truth_E;
    for ( unsigned long int index = 0; index < truthPdgId->size(); ++index ) {
      if ( truthPdgId->at(index) == 3000001 ) { // ID of DPs

        truth_pT.push_back(truthPt->at(index)); // for LR test

        truth_x.push_back(truthDecayVtx_x->at(index));
        truth_y.push_back(truthDecayVtx_y->at(index));
        truth_z.push_back(truthDecayVtx_z->at(index));
        truth_E.push_back(truthE->at(index));
      }
    }

    // std::cout << truth_x.size() << std::endl; // this showed that not all events have 2 DPs
    if (truth_x.size() != 2) continue;

    // Lifetime reweighting
    // DPJs are in indexes 3 and 5 in truthPdgId (not always!)
    //ct1 = ctau(truthDecayVtx_x->at(3),truthDecayVtx_y->at(3),truthDecayVtx_z->at(3),truthE->at(3),m_dpj_500757);
    //ct2 = ctau(truthDecayVtx_x->at(5),truthDecayVtx_y->at(5),truthDecayVtx_z->at(5),truthE->at(5),m_dpj_500757);

    // ct1 is leading, ct2 is subleading, for LR test
    //if (truth_pT.at(0) > truth_pT.at(1)) {
    //  ct1 = ctau(truth_x.at(0),truth_y.at(0),truth_z.at(0),truth_E.at(0),m_dpj_521259); // CHANGE MASS FOR LIFETIME REWEIGHTING
    //  ct2 = ctau(truth_x.at(1),truth_y.at(1),truth_z.at(1),truth_E.at(1),m_dpj_521259);
    //}
    //else {
    //  ct1 = ctau(truth_x.at(1),truth_y.at(1),truth_z.at(1),truth_E.at(1),m_dpj_521259); // CHANGE MASS FOR LIFETIME REWEIGHTING
    //  ct2 = ctau(truth_x.at(0),truth_y.at(0),truth_z.at(0),truth_E.at(0),m_dpj_521259);
    //}

    ct1 = ctau(truth_x.at(0),truth_y.at(0),truth_z.at(0),truth_E.at(0),m_dpj_521263); // CHANGE MASS FOR LIFETIME REWEIGHTING
    ct2 = ctau(truth_x.at(1),truth_y.at(1),truth_z.at(1),truth_E.at(1),m_dpj_521263);

    //std::cout << ct1 << std::endl; // check ct1,2 values

    w1 = newWeight(ctau_gen_521263,newctau,ct1); // CHANGE FOR LIFETIME REWEIGHTING
    w2 = newWeight(ctau_gen_521263,newctau,ct2);

    m_weight = scale1fb * intLumi * metTrig_weight * w1 * w2; // * w1 * w2, saving reweighted events in output file
    absdphijj = fabs(dphijj);

    // Yanyan's suggestion to remove potentially highly weighted events
    //if (ct1+ct2 < 230) continue; // in mm

    // Cristiano's suggestion - matching DPJ to truth DPs
    if (LJjet1_truthDPidx == -999) continue; // keep only events where the leading DPJ is truth-matched to a dark photon
    //if (LJjet1_truthDPidx == 3 || LJjet1_truthDPidx == 5) continue; // keep only events where the leading DPJ is not truth-matched to a dark photon

    // Cuts
    // DSID cut to veto low pT QCD
    //if (dsid == 364702) continue;
    //count[0]+=1;
    //total_weight[0]+=m_weight;

    // VBF jets
    if (njet30<1 || mjj<1000000 || fabs(detajj)<3) continue;
    count[0]+=1;
    total_weight[0]+=m_weight;
    sum_w2[0]+=m_weight*m_weight;

    // Leading jet pT cut
    //if (jet1_pt < 90000) continue;
    //count[2]+=1;
    //total_weight[2]+=m_weight;

    // metTrig
    if (metTrig == false) continue;
    count[1]+=1;
    total_weight[1]+=m_weight;
    sum_w2[1]+=m_weight*m_weight;

    // MET cut
    //if (MET < 225000) continue; // high MET SR
    if (MET < 100000 || MET > 225000) continue; // low MET SR
    count[2]+=1;
    total_weight[2]+=m_weight;
    sum_w2[2]+=m_weight*m_weight;

    // LJmus & LJjets
    //if (nLJmus20 != 0 || nLJjets20 < 1) continue;
    if (LJ1_type != 2) continue;
    count[3]+=1;
    total_weight[3]+=m_weight;
    sum_w2[3]+=m_weight*m_weight;

    // gapRatio & BIBtagger cuts
    if (LJjet1_gapRatio < 0.9 || LJjet1_BIBtagger < 0.2) continue;
    count[4]+=1;
    total_weight[4]+=m_weight;
    sum_w2[4]+=m_weight*m_weight;

    // Signal prompt lepton veto
    if (neleSignal != 0 || nmuSignal != 0) continue;
    count[5]+=1;
    total_weight[5]+=m_weight;
    sum_w2[5]+=m_weight*m_weight;

    // b-jet veto
    if (hasBjet != 0) continue;
    count[6]+=1;
    total_weight[6]+=m_weight;
    sum_w2[6]+=m_weight*m_weight;

    // min_dphi_jetmet cut
    if (min_dphi_jetmet < 0.4) continue;
    count[7]+=1;
    total_weight[7]+=m_weight;
    sum_w2[7]+=m_weight*m_weight;

    // JVT cut
    if (LJjet1_jvt > 0.4) continue; // 0.4
    count[8]+=1;
    total_weight[8]+=m_weight;
    sum_w2[8]+=m_weight*m_weight;

    // dphijj cut
    if (fabs(dphijj) > 2.5) continue;
    count[9]+=1;
    total_weight[9]+=m_weight;
    sum_w2[9]+=m_weight*m_weight;

    // Timing cut
    if (fabs(LJjet1_timing) > 4) continue;
    count[10]+=1;
    total_weight[10]+=m_weight;
    sum_w2[10]+=m_weight*m_weight;

    // ABCD plane DPJtagger range
    if (LJjet1_DPJtagger < 0.8 || LJjet1_DPJtagger > 1) continue;
    count[11]+=1;
    total_weight[11]+=m_weight;
    sum_w2[11]+=m_weight*m_weight;

    // ABCD plane isoID range
    if (LJjet1_isoID < 0 || LJjet1_isoID > 20000) continue;
    count[12]+=1;
    total_weight[12]+=m_weight;
    sum_w2[12]+=m_weight*m_weight;

    // ABCD SR
    if (LJjet1_isoID >= 2000 || LJjet1_DPJtagger < 0.9) continue;
    count[13]+=1;
    total_weight[13]+=m_weight;
    sum_w2[13]+=m_weight*m_weight;

    // Fill output tree
    tree_out->Fill();
    
  } // Event Loop

  std::cout << "--- End of event loop ---" <<std::endl;
 
}

std::vector<double> AnalysisInput::finalize()
{

  // MC Events after cuts
  for (int icount=0; icount<n_counters; icount++) {

    TString NameMC = "MC events after ";
    if (icount==0) NameMC +=  "VBF jets cuts                     ";
    if (icount==1) NameMC +=  "metTrig                           ";
    if (icount==2) NameMC +=  "MET cut                           ";
    if (icount==3) NameMC +=  "Leading LJ type calo              ";
    if (icount==4) NameMC +=  "gapRatio & BIBtagger cut          ";
    if (icount==5) NameMC +=  "Prompt lepton veto                ";
    if (icount==6) NameMC +=  "b-jet veto                        ";
    if (icount==7) NameMC +=  "min_dphi_jetmet cut               ";
    if (icount==8) NameMC +=  "JVT cut                           ";
    if (icount==9) NameMC +=  "abs(dphijj) cut                   ";
    if (icount==10) NameMC += "Timing cut                        ";
    if (icount==11) NameMC += "ABCD DPJtagger range              ";
    if (icount==12) NameMC += "ABCD isoID range                  ";
    if (icount==13) NameMC += "ABCD SR                           ";

    std::cout<< NameMC + ": "<< count[icount] <<std::endl;

    //if ( option_ == sgn_VBF )    MCEv_VBF << icount << "," << count[icount] << std::endl;
    //if ( option_ == data15 )  MCEv_Run2 << icount << "," << count[icount] << std::endl;

  }

  std::cout<< "---------------------------------------------------------------" <<std::endl;

  // Events after cuts
  for (int icount=0; icount<n_counters; icount++) {

    TString NameEv = "Events after ";
    if (icount==0) NameEv +=  "VBF jets cuts                        ";
    if (icount==1) NameEv +=  "metTrig                              ";
    if (icount==2) NameEv +=  "MET cut                              ";
    if (icount==3) NameEv +=  "Leading LJ type calo                 ";
    if (icount==4) NameEv +=  "gapRatio & BIBtagger cut             ";
    if (icount==5) NameEv +=  "Prompt lepton veto                   ";
    if (icount==6) NameEv +=  "b-jet veto                           ";
    if (icount==7) NameEv +=  "min_dphi_jetmet cut                  ";
    if (icount==8) NameEv +=  "JVT cut                              ";
    if (icount==9) NameEv +=  "abs(dphijj) cut                      ";
    if (icount==10) NameEv += "Timing cut                           ";
    if (icount==11) NameEv += "ABCD DPJtagger range                 ";
    if (icount==12) NameEv += "ABCD isoID range                     ";
    if (icount==13) NameEv += "ABCD SR                              ";

    std::cout<< NameEv + ": "<< total_weight[icount] <<std::endl;

    //if ( option_ == sgn_VBF )    Ev_VBF << icount << "," << total_weight[icount] << std::endl;
    //if ( option_ == data15 )  Ev_Run2 << icount << "," << total_weight[icount] << std::endl;

  }

  std::cout<<"---------------------------------------------------------------" << std::endl;
  std::cout << "Total number of events at 140 fb^-1: " << total_weight[n_counters-1] << " +- " << sqrt(sum_w2[n_counters-1]) << std::endl;
  std::cout << "Reweighted eff x acc at 140 fb^-1  : " << total_weight[n_counters-1]/52920 << " +- " << sqrt( sum_w2[n_counters-1]/(pow(52920,2)) + ((total_weight[n_counters-1]*total_weight[n_counters-1])/(pow(52920,4)))*52920 ) << std::endl; // error propagation assuming sqrt(N) for denominator

  // Write and close output file
  output->Write();
  output->Close();
  
  // finalize
  std::cout << "--- Finalize ---" <<std::endl;
  
  return { total_weight[n_counters-1]/52920, sqrt( sum_w2[n_counters-1]/(pow(52920,2)) + ((total_weight[n_counters-1]*total_weight[n_counters-1])/(pow(52920,4)))*52920 ) };

}
