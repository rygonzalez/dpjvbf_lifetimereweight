# dDPJ VBF Lifetime Reweighting

Implementation of [simple lifetime reweighting algorithm](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/LifetimeReweighting) to extrapolate the signal efficiency times acceptance after event selection.

The analysis ntuples needed as input can be found at `/eos/user/y/yygao/DarkPhoton/miniT/vbfskim/v01-06/`. Download them and change the ntuples path within the header file `source/lib/AnalysisInput.h`.

## Running the code locally

The code runs over a single signal sample at a time, calculating a new weight in a per-event basis to obtain a reweighted efficiency. To run over a different mass point, change manually the input file in `source/lib/AnalysisInput.cxx`.

Create a build folder and compile the source code:
```
cd DPJVBF_LifetimeReweight/
mkdir build samples
cd build/
cmake ../source/
make
```
The `samples` folder is used to store an output root file with the new event weights, but we're not using it here since the reweighted efficiencies are written to a csv file.

Within the `build` folder, execute the code directly by running:
```
./RunInput
```

### Plotting

A jupyter notebook is used to create the efficiency curves as a function of the dark photon proper decay length. This takes the output csv files (one per mass point) and creates an ATLAS-style plot using `matplotlib`.

To run and edit the code, do:
```
jupyter-notebook plotEffs.ipynb
```

## Running the code in lxplus

If compiling the code locally does not work (e.g., due to lacking libraries or using different compiler versions), running it in `lxplus` is always an option.

For this, login to `lxplus` using the `ssh` protocol as:
```
ssh -XY USERNAME@lxplus.cern.ch
```
Once in, you can clone the repository as usual (`git clone https://gitlab.com/rygonzalez/DPJVBF_LifetimeReweight.git`) and modify the necessary paths in the `AnalysisInput.h` and `AnalysisInput.cxx` codes.

Proceed to setup your environment as:
```
setupATLAS
lsetup cmake
lsetup "root 6.22.00-python3-x86_64-centos7-gcc8-opt"
```
This setups `cmake 3.21.3` and a root version compiled with `python3`.

At this point, it is necessary to modify the `CMakeLists.txt` file located in the source folder, specifying the use of `c++17` or more recent versions of the C++ standard. For this, uncomment line 8 in the file and change `11` to `17`; which should look like:
```
set (CMAKE_CXX_STANDARD 17)
```

With this, compile the code following the previous instructions as if you were working on your local machine.

## Useful links
* [VBF DPJ Analysis Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/VBFDPJAnalysis)
